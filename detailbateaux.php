<?php ob_start(); ?>
<?php session_start(); ?>

<?php

$idBateau = 0;
//Tester si les variables POST existent
if (isset($_GET["idBateau"])) {
    $idBateau = intval(htmlspecialchars($_GET['idBateau']));
}
//Requete SQL
require "bdd/bddconfig.php";
$objBdd = new PDO("mysql:host=$bddserver;dbname=$bddname;charset=utf8", $bddlogin, $bddpass);
$objBdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$detailBateau = $objBdd->prepare("SELECT * FROM bateau WHERE idBateau = :id");
$detailBateau->bindParam(':id', $idBateau, PDO::PARAM_INT);
$detailBateau->execute();

$skipper = $objBdd->prepare("SELECT * FROM skipper WHERE idBateau = :id");
$skipper->bindParam(':id', $idBateau, PDO::PARAM_INT);
$skipper->execute();
?>

<?php
while ($temp = $detailBateau->fetch()) {
?>

<h2> <?php echo $temp['nomBateau'] ?> </h2>
<img src="images/bateaux/<?php echo $temp['photo'] ?>">
<?php
} //fin du while
$detailBateau->closeCursor(); //libère les ressources de la bdd
?>

<?php
while ($temp1 = $skipper->fetch()) {
?>

<h2> <?php echo $temp1['nomSkipper'] ?> </h2>
<img src="images/skippers/<?php echo $temp1['photo'] ?>">
<?php
} //fin du while
$skipper->closeCursor(); //libère les ressources de la bdd
?>

<?php $contenu = ob_get_clean(); ?>
<?php require 'gabarit/templates.php' ?>