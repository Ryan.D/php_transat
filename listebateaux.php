<?php ob_start(); ?>
<?php session_start(); ?>

<?php

$idClasse = 0;
//Tester si les variables POST existent
if (isset($_GET["idClasse"])) {
    $idClasse = intval(htmlspecialchars($_GET['idClasse']));
}
//Requete SQL
require "bdd/bddconfig.php";
$objBdd = new PDO("mysql:host=$bddserver;dbname=$bddname;charset=utf8", $bddlogin, $bddpass);
$objBdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$bateau = $objBdd->prepare("SELECT * FROM bateau WHERE idClasse = :id ORDER BY classementFinal");
$bateau->bindParam(':id', $idClasse, PDO::PARAM_INT);
$bateau->execute();
?>
<table>
    <thead>
        <tr>
            <th colspan="2">Classes</th>
        </tr>
    </thead>
    <tbody>
            <?php
            while ($temp = $bateau->fetch()) {
            ?>
                <tr>
                <?php 
                if ($temp['classementFinal'] == 9999) {
                    $temp['classementFinal'] = 'AB';
                }
                ?>
                    <td><?php echo $temp['classementFinal'] ?></td>
                    <td><a href="detailbateaux.php?idBateau=<?php echo $temp['idBateau'] ?>"><?php echo $temp['nomBateau'] ?></a></td>
                </tr>

            <?php
            } //fin du while
            $bateau->closeCursor(); //libère les ressources de la bdd
            ?>
    </tbody>
</table>

<?php $contenu = ob_get_clean(); ?>
<?php require 'gabarit/templates.php' ?>