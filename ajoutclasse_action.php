<?php require "bdd/bddconfig.php";
session_start();

$paramOK = false;
if ((isset($_POST['nomClasse'])) && (isset($_POST['dateDepart'])) && (isset($_POST['engagee'])) && (isset($_POST['typeCoque'])) && (isset($_POST['dateFinCourse'])) && (isset($_POST['dateProchainClassement'])) && (isset($_POST['tailleCoque']))) {
    $nomClasse = htmlspecialchars($_POST['nomClasse']);
    if (htmlspecialchars($_POST['dateDepart']) != '') {
        $dateDepart = htmlspecialchars($_POST['dateDepart']);
    }
    $engagee = htmlspecialchars($_POST['engagee']);
    $typeCoque = htmlspecialchars($_POST['typeCoque']);
    if (htmlspecialchars($_POST['dateFinCourse']) != '') {
        $dateDepart = htmlspecialchars($_POST['dateFinCourse']);
    }
    if (htmlspecialchars($_POST['dateProchainClassement']) != '') {
        $dateDepart = htmlspecialchars($_POST['dateProchainClassement']);
    }
    $tailleCoque = intval(htmlspecialchars($_POST['tailleCoque']));
    $paramOK = true;
}

// INSERT dans la base
if ($paramOK == true) {
    $objBdd = new PDO("mysql:host=$bddserver;dbname=$bddname;charset=utf8", $bddlogin, $bddpass);
    $objBdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    $addBateau = $objBdd->prepare("INSERT INTO classebateau (nomClasse,dateDepart,engagee,typeCoque,dateFinCourse,dateProchainClassement,tailleCoque) VALUES (:nomClasse,:dateDepart,:engagee,:typeCoque,:dateFinCourse,:dateProchainClassement,:tailleCoque)");
    $addBateau->bindParam(':nomClasse', $nomClasse, PDO::PARAM_STR);
    $addBateau->bindParam(':dateDepart', $dateDepart, PDO::PARAM_INT);
    $addBateau->bindParam(':engagee', $engagee, PDO::PARAM_STR);
    $addBateau->bindParam(':typeCoque', $typeCoque, PDO::PARAM_STR);
    $addBateau->bindParam(':dateFinCourse', $dateFinCourse, PDO::PARAM_INT);
    $addBateau->bindParam(':dateProchainClassement', $dateProchainClassement, PDO::PARAM_INT);
    $addBateau->bindParam(':tailleCoque', $tailleCoque, PDO::PARAM_INT);
    $addBateau->execute();

    $lastId = $objBdd->lastInsertId();
    echo $lastId;
}

$serveur = $_SERVER['HTTP_HOST'];
$chemin = rtrim(dirname(htmlspecialchars($_SERVER['PHP_SELF'])), '/\\');
$page = 'index.php';
header("Location: http://$serveur$chemin/$page");
