<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <?php $title = 'Transat Jacques Vabre 2007' ?>
    <title> <?php echo $title ?> </title>
    <link rel="stylesheet" href="css/transat.css" />
</head>

<body>
    <div id="conteneur">
        <header>
            <div class='login'>
                <form method="POST" action="login_action.php">
                    <fieldset>
                        <?php if (isset($_SESSION['logged_in']['login']) != '') { ?>
                            <legend>Log-Out</legend>
                            <a href="logout.php">Vous-deconnectez</a>
                        <?php
                        } else { ?>
                            <legend>Connectez-vous</legend>
                            <input type="text" name="login" value="" placeholder="Login" required>
                            <input type="text" name="password" value="" placeholder="Password" required>
                            <input type="submit" value="Valider">
                        <?php
                        } ?>
                    </fieldset>
                </form>
            </div>
        </header>




        <nav>
            <ul>
                <li><a href="index.php" class="navitem">Accueil</a></li>
                <li><a href="classements.php" class="navitem">Classements</a></li>

                <?php if (isset($_SESSION['logged_in']['fonction']) == 'admin') { ?>

                    <li><a href="ajoutclasse.php" class="navitemprivate">Ajout Classe</a></li>
                <?php
                } ?>

            </ul>
        </nav>
        <section>
            <article>
                <?php echo $contenu; ?>
            </article>
        </section>
        <footer>
            <p>Copyright Moi - Tous droits réservés -
                <a href="#">Contact</a>
            </p>
        </footer>
    </div>
</body>

</html>