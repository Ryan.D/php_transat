<?php ob_start(); ?>
<?php session_start(); ?>

<?php
//Requete SQL
require "bdd/bddconfig.php";
$objBdd = new PDO("mysql:host=$bddserver;dbname=$bddname;charset=utf8", $bddlogin, $bddpass);
$objBdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$classeBateau = $objBdd->query("SELECT * FROM classebateau ORDER BY typeCoque DESC");
?>

<table>
    <thead>
        <tr>
            <th colspan="2">Classes</th>
        </tr>
    </thead>
    <tbody>
            <?php
            while ($temp = $classeBateau->fetch()) {
            ?>
                <tr>
                    <td><?php echo $temp['typeCoque'] ?></td>
                    <td><a href="listebateaux.php?idClasse=<?php echo $temp['idClasse'] ?>"  > <?php echo $temp['nomClasse'] ?> </a> </td>
                </tr>

            <?php
            } //fin du while
            $classeBateau->closeCursor(); //libère les ressources de la bdd
            ?>
    </tbody>
</table>


<?php $contenu = ob_get_clean(); ?>
<?php require 'gabarit/templates.php' ?>