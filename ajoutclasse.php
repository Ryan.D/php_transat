<?php ob_start(); ?>
<?php session_start(); ?>

<form action="ajoutclasse_action.php" method="POST" class="ajout">
    <label for="">Nom de la classe</label>
    <input type="text" name="nomClasse">
    <label for="">Date de depart</label>
    <input type="date" name="dateDepart">
    <label for="">Engagée ?</label>
    <select name="engagee">
        <option value="oui">oui</option>
        <option value="non">non</option>
    </select>
    <label for="">Type de coque</label>
    <select name="typeCoque">
        <option value="Monocoque">Monocoque</option>
        <option value="Multicoque">Multicoque</option>
    </select>
    <label for="">Date de fin de course</label>
    <input type="date" name="dateFinCourse">
    <label for="">Date du prochaine classement</label>
    <input type="date" name="dateProchainClassement">
    <label for="">Taille de la coque</label>
    <input type="text" name="tailleCoque">
    <input type="submit" value="Valider">
</form>

<?php $contenu = ob_get_clean(); ?>
<?php require 'gabarit/templates.php' ?>